# ![Tapbuy](https://drive.google.com/uc?id=0B4nM6cjFIhuRRTVDSnZHMW40TDg =200x)

# Tapbuy SDK for iOS

Tapbuy SDK for iOS is the easiest way to use Tapbuy in your iOS app : sell your products through the Tapbuy express checkout solution.

This repository stores the SDK release archives. Please refer to [docs.tapbuy.io](https://docs.tapbuy.io) for more information.